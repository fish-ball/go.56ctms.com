/**************************
*   author Jay chen
*   Produced in 2016-8-30
***************************/
//模板
$('#copy').html('版权所有·中城物流&nbsp;&nbsp;&nbsp;&nbsp;京ICP备16043154号-2&nbsp;&nbsp;&nbsp;&nbsp;Copyright &copy; 2018 zhiyunstar.com All Rights Reserved');//版权信息
$('#head').append('<div class="tel r"><img src="'+publicRoot+'/img/logo/tel.png" alt=""></div>')//咨询电话

$.cookie('ttms') && $.cookie('ttms') != '' && (function(){
	window.location.href = '/frame?v='+bops.verson;
})()

if(window.location.href.indexOf('com') > -1){
    //外网
    var redirectUri = 'http://connect.56ctms.com/wechat/bind';
    $('.wechart').attr('href','http://passport.56ctms.com/passport/wechat?redirectUri='+redirectUri+'&returnUri='+'http://' + host +'/ttms/jump')
}else{
	var redirectUri = 'http://localhost:5089/wechat/bind';
    $('.wechart').attr('href','http://user.test.56ctms.com/passport/wechat?redirectUri='+redirectUri+'&returnUri='+'http://' + host +'/ttms/jump')
}
//注册start
var registerUrlMap = {
	'localhost':'http://localhost:9087/',
	'test.56ctms.com':'http://account.test.56ctms.com/',
	'go3tms.test.56ctms.com':'http://account.test.56ctms.com/',
	'yf.56ctms.com':'http://account.yf.56ctms.com/',
	'3tms.yf.56ctms.com':'http://account.yf.56ctms.com/',
	'ctms.kuaihuoyun.com':'http://account.56ctms.com/',
	'go.56ctms.com':'http://account.56ctms.com/',
	'goctms.kuaihuoyun.com':'http://account.56ctms.com/',
	'go3tms.kuaihuoyun.com':'http://account.56ctms.com/',
	'tms.g2link.cn':'http://account.tms.g2link.cn/',
	'3tms.wetrans.group':'http://account.3tms.wetrans.group/'
}
var registerUrlDev = ['localhost','www.test.com','127.0.0.1'];
var domainHost = document.domain;
var reurl = '';
if(registerUrlMap[domainHost]){
	if(registerUrlDev.indexOf(domainHost) > -1){
		reurl = 'http://'+domainHost+':9086';
	}else{
		reurl = 'http://'+domainHost;
	}
	$('#go_register').attr('href',registerUrlMap[domainHost]+'?reurl='+reurl).show();
	$('#forget_pass').attr('href',registerUrlMap[domainHost]+'?reurl='+reurl+'&type=repass')
}else{
	$('#go_register').remove()
}

//注册end
$('.placeholder2').each(function(){
	if($(this).val() != ''){
		$(this).next('.pass-bottom').hide();
		$(this).parents('.phone-inp2').find('.checks').hide();
	}
})

$("body").on("focus",".placeholder2",function() {
	$(this).next('.pass-bottom').hide();
	$(this).parents('.phone-inp2').find('.checks').hide();
});

$("body").on("blur",".placeholder2",function() {
	if($(this).val() == ''){
		$(this).next('.pass-bottom').show();
		$(this).parents('.phone-inp2').find('.checks').show();
	}
});
if(!$.cookie('ttms_captcha_certificate')){
    setTimeout(function () {
        $('.res').attr('src','/ttms/captcha/get_image_code?t='+Math.random());
        $('.res').show();
    },300)
}else{
    $('.res').show();
}
$('.res').on('click',function(){
	$(this).attr('src','/ttms/captcha/get_image_code?t='+Math.random())
})
function checkMobile(text){
    var myreg = /^(1+\d{10})$/;
    return !myreg.test(text) ? false : true;
}
function loginIn(obj){//登录
	var type = true;
	$('.placeholder2').each(function(){
		if($(this).val() == ''){
			$(this).parents('.phone-inp2').find('.checks').show();
			type = false;
		}
	})
	var phones = $('#phone-inp').val();
	!checkMobile(phones) && (function(){
		$('.check-phone').show();
		type = false;
	})()
	if(type){
		if($(obj).html() == '登录'){
			$(obj).html('登录中..');
			$.ajax({
		    	type:"post",
		    	url:'/ttms/account/login',
		    	data:{
					phone : $('#phone-inp').val(),
					password : $('#password-inp').val(),
					code : $('#code-inp').val(),
				},
		    	dataType:"json",
		    	success:function(data){
		    		if(data.status == 200){
		    			window.location.href = '/ttms/chose.html?ishttp=yes'
		    		}else{
                        if(data.status == 1001){
                            $('.check-code').show();
                            $('#code-inp').select();
                        }else if(data.status == 1004){
                            $('.check-phone').show()
                        }else if(data.status == 1006){
                            $('.check-pas').show()
                        }else if(data.status == 1002){
                            $('#code-inp').select();
                            success_alert(data.message || '服务器异常，请重试！', 'error');
                        }else{
                            success_alert(data.message || '服务器异常，请重试！', 'error');
                        }
                        $('.res').attr('src','/ttms/captcha/get_image_code?t='+Math.random())
                    }
		    		$(obj).html('登录');
		    	},
		    	error:function(){
		    		success_alert('服务器异常，请重试','error');
		    		$(obj).html('登录');
		    	}
		    })
		}

	}
}
var once = 1;
$(document).on('click',function(){
	if(once == 1){
		$(document).on('keydown',function(){
			event.keyCode == 13 && (function(){
				if($('#phone-inp').is(':focus')){
					$('#password-inp').focus();
				}else if($('#password-inp').is(':focus')){
					$('#code-inp').focus();
				}else{
					$('.login-btn').click();
				}
			})()
				
			
		})
		once = 0;
	}	
})
function success_alert(text,type){
	$('#success-error').remove();
	var html = '';
	if(type == 'error'){
		var bac = 'background-color:#ff6666;';
	}else{
		var bac ='';
	}
	var width = 'auto'
	html += '<div id="success-error" style="width:'+width+';margin-left:-'+width/2+'px;'+bac+'">';
	html += '<p>'+text+'</p>';
	html += '</div>';
	$('body').append(html);
	var width2 = $('#success-error').outerWidth();
	$('#success-error').css('margin-left',-width2/2 + 'px');
	setTimeout(function(){
		$('#success-error').fadeOut(300);
		setTimeout(function(){
			$('#success-error').remove()
		},300)
	},1500);
}
var _vds = _vds || [];
window._vds = _vds;
(function(){
	_vds.push(['setAccountId', 'b46c9374508ad029']);
	(function() {
		var vds = document.createElement('script');
		vds.type='text/javascript';
		vds.async = true;
		vds.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'dn-growing.qbox.me/vds.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(vds, s);
	})();
})();
