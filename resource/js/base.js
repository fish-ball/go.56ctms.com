/**************************
*   author Jay chen
*   Produced in 2016-8-30
***************************/
var $tbody = $('.table tbody');
var $thead = $('.table thead');
/* 设置地图布局 */
function initLay() {
    var h = $(window).height() - 20;
    $("#container").css("height", h + 'px');
}
// table高度及table搜索左右适应适应
function auto(hei) {
    var widthSer;
    function getChildrenWidth(){
        var w1 = 0;
        var $a = $('.table-head .footer-table-right').find('a');
        $a.each(function(){
            w1 += $(this).outerWidth(true);
        });
        return w1;
    }
    if(getChildrenWidth() == 0){
        widthSer = $('.table-head .footer-section').outerWidth() - getChildrenWidth();
    }else if(getChildrenWidth() >= 0){
        widthSer = $('.table-head .footer-section').outerWidth() - getChildrenWidth() - 50;
    }

    $('.table-head .footer-table-left').css('width', widthSer + 'px'); //table-head左侧换行，右侧固定
    var heightTab = $(window).height() - $('.table-head').outerHeight() - $('.table-foot').outerHeight() - $('.table-nav').outerHeight() - $('.table-tatol').outerHeight() - $('.floatFoot').outerHeight() - $('.outuser-head').outerHeight() - $('.outgoing').outerHeight() - 30;
    $('.tab-box').css('height', heightTab + 'px');  //table自适应

    $('.floatFir').css('max-height', heightTab - 12 + 'px');//定义左侧悬浮高度自适应
}
auto();
$(window).on('resize', function () {
    auto();
    initLay();
});

//固定表头
function mainTableLaod(unReflow) {//unReflow存在就不做自适应
    $("#mainTable").length && $("#mainTable").floatThead({
        position: 'absolute',
        scrollContainer: function ($table) {
            return $(".tab-box");
        },
        autoReflow: unReflow ? false : true  //自适应td长度
    });
    // $('.floatThead-table').css({ 'border-color': '#e5e5e5' });
    // $('.floatThead-container').css('border-bottom', '1px solid #7bb7ff');
    $('.floatThead-container').css('border-bottom', 'none');
    $('.floatThead-table th').css({ 'border': 'none' });
}

//公用ajax请求方法
var defaultsOpt = {};
function ajaxPost(url, postOpt, callSuccess, callError) {
    ajaxPublic('post',url, postOpt, callSuccess, callError)
}
function ajaxGet(url, postOpt, callSuccess, callError) {
    ajaxPublic('get',url, postOpt, callSuccess, callError)
}
function ajaxPublic(type,url, postOpt, callSuccess, callError) {
    var options = $.extend({}, defaultsOpt, postOpt);   //$.extend({},a,b)方法是合并{}之后的a,b,c...对象成为一组数组对象；
    $.ajax({
        type: type,
        url: url,
        data: options,
        dataType: "json",
        success: function(data){
            if(data.status == 401){
                if(typeof clearOut == 'function'){
                    clearOut()
                }else{
                    parent.clearOut();
                }
            }else if(data.status == 402){
                if(typeof clearChose == 'function'){
                    clearChose()
                }else{
                    parent.clearChose();
                }
            }else if(data.status == 403){
                if(typeof clearUnallow == 'function'){
                    clearUnallow()
                }else{
                    parent.clearUnallow();
                }
            }else{
                callSuccess(data);
            }
            unloading();
        },
        error: callError || errorFun
    })
}
function errorFun() {
    unloading();
    success_alert('服务器异常，请重试！', 'error');
}

//截取url带过来的参数
function getvl(name) {
    var reg = new RegExp("(^|\\?|&)" + name + "=([^&]*)(\\s|&|$)", "i");
    return reg.test(location.href) ? decodeURI(RegExp.$2.replace(/\+/g, " ")) : '';
};

// 清空
function resetSearch(type) {
    //下拉组织机构请款
    $('#jMenu li a.fNiv').attr('organizeid', '')
    $('#jMenu li a.fNiv').attr('organize-nid', '')
    $('#jMenu li a.fNiv span').text('全部')

    $("input.layui-input,select.layui-input,input.input-text,input.input-text2,select.select").val("");
    $('textarea.textarea-text').val("");
    $('.input-text-check').prop('checked', false);
    $('.input-text-check2').prop('checked', true);
    $('.khy-address').attr('data-lng', 0).attr('data-lat', 0);
    $('#chose_save_in').html('未选择司机').attr('data-id', '').attr('data-type', '').attr('data-send', '');
    $("#startTime").hasClass("date-input") &&
        (function () {
            $("#startTime").datepicker("setDate", "-30d");
            $("#endTime").datepicker("option", "maxDate", "0d");
            $("#endTime").datepicker("setDate", "0d");
            $("#ifTime").datepicker("setDate", "0d");
            $("#ifTime").datepicker("option", "minDate", "0d");
            $("#start").datepicker("setDate", "+0d");
        })()
    $("#startTime2").hasClass("date-input") &&
        (function () {
            $("#startTime2").datepicker("setDate", "-30d");
            $("#endTime2").datepicker("option", "maxDate", "0d");
            $("#endTime2").datepicker("setDate", "0d");
            $("#ifTime2").datepicker("setDate", "0d");
            $("#ifTime2").datepicker("option", "minDate", "0d");
            $("#start2").datepicker("setDate", "+0d");
        })()
    $("#month_input").hasClass("date-input") && monthLoad();
    if (type == 'setHour') {
        setHour();
        $('.main-first-right').parent('.clearfix').each(function (i) {
            if (i > 0) {
                $(this).remove();
            }
        })
    }
}
//判断对象的长度
Object.size = function (obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

// 搜素
function searches() {
    $tbody.html('');
    $('.loading').attr('data-load', 'yes');
    $('.load-gif').show();
    $('.load-none').hide();
    loadData(1);
}
// 转换字符串成时间
function changeTime(t, nohour) {
    if (String(t).indexOf("-") >= 0 || t == null) {
        return t = '';
    } else {
        var date = new Date(t);
        Y = date.getFullYear() + '-';
        M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
        D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
        h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
        m = date.getMinutes() < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':';
        s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
        return nohour ? Y + M + D : Y + M + D + h + m + s;
    }
}
//tab标签遮罩
$('body').on('click', '.nav-tab li', function () {
    if ($(this).find('.tab-zhe').attr('data-allow') != 'no') {
        $('.nav-tab li').removeClass('active').find('.tab-zhe').show().attr('data-allow', 'no');
        $(this).addClass('active');
    }
})

//没有更多可加载的列表
function noMore() {
    $('.loading').attr('data-load', 'no');
    $('.load-gif').hide();
    $('.load-none').show();
}


//处理键盘事件  
function doKey(e) {
    var ev = e || window.event;//获取event对象  
    var obj = ev.target || ev.srcElement;//获取事件源  
    var t = obj.type || obj.getAttribute('type');//获取事件源类型  
    if (ev.keyCode == 8 && t != "password" && t != "text" && t != "textarea") {
        return false;
    }
}
//禁止后退键 作用于Firefox、Opera  
document.onkeypress = doKey;
//禁止后退键  作用于IE、Chrome  
document.onkeydown = doKey;

//js模拟点击事件
function JsClick(id) {
    if (document.all) {
        document.getElementById(id).click();
    }
    else {
        var event = document.createEvent("MouseEvents");
        event.initEvent("click", true, true);
        document.getElementById(id).dispatchEvent(event);
    }
}

//纯数字且不能输入空格且不能输入小数点
// isZero == zero可为零
// isZero == unNone不能为空格
function keepNum(obj, num, isZero) {
    var $this = $(obj);
    var v = $this.val();
    if (isNaN(v) || (v.indexOf('.') != -1)) {
        var values = v.slice(0, -1);
        if (isNaN(values)) {
            values = ''
        }
        values = values.replace(/\s/g, "");
        $this.val(values);
    } else {  //去除空格
        $this.val(v.replace(/\s/g, ""));
    }
    num && (function () {
        var newValue = $this.val();
        if (!isNaN(newValue) && newValue != '') {
            if (isZero == 'zero') {
                if (newValue < 0) {
                    newValue = 0;
                } else if (newValue > num) {
                    newValue = num;
                }
            } else {
                if (newValue < 1) {
                    newValue = 1;
                } else if (newValue > num) {
                    newValue = num;
                }
            }

        } else {
            if (isZero == 'unNone') {
                if (newValue < 1) {
                    newValue = 1;
                } else if (newValue > num) {
                    newValue = num;
                }
            }
        }
        $this.val(newValue);
    })()
}
//纯数字且不能输入空格且不能输入小数点可以是负数   默认没有小数
function keepSR(obj, num,maxlength) {
    var imposeNum = maxlength ? (maxlength+1) : 1;
    var $this = $(obj);
    var v = $this.val();
    if (isNaN(v)) {
        if(v!='-'){
            var values = v.slice(0, -1);
            if (isNaN(values)) {
                values = ''
            }
            values = values.replace(/\s/g, "");
            $this.val(values);
        }   
    } else if (v.indexOf('.') != -1) {
        if (v.split('.')[1].length == imposeNum) {
            var values = v.slice(0, -1);
            $this.val(values);
        } else if (v.split('.')[1].length > imposeNum) {
            $this.val('');
        }
    }else {  //去除空格
        $this.val(v.replace(/\s/g, ""));
    }
    num && (function () {
        var newValue = $this.val();
        if (!isNaN(newValue) && newValue != '') {
            if (newValue < -num) {
                newValue = -num;
            } else if (newValue > num) {
                newValue = num;
            }
        }
        $this.val(newValue);
    })()
}
//数字且最限制小数点位数，（默认三位）且不能输入空格且可以为0
function keepSH(obj, num ,maxlength) {
    var $this = $(obj);
    var v = $this.val();
    var imposeNum = maxlength ? (maxlength+1) : 4;//限制小数点数量，默认为小数点后3位
    if (isNaN(v)) {
        var values = v.slice(0, -1);
        if (isNaN(values)) {
            values = ''
        }
        $this.val(values);
    } else if (v.indexOf('.') != -1) {
        if (v.split('.')[1].length == imposeNum) {
            var values = v.slice(0, -1);
            $this.val(values);
        } else if (v.split('.')[1].length > imposeNum) {
            $this.val('');
        }
    } else {
        $this.val(v.replace(/\s/g, ""));
    }

    var newValue = $this.val();
    if (!isNaN(newValue) && newValue != '') {
        if (newValue > num) {
            newValue = num;
        }
    }
    $this.val(newValue);
}

function keepSH2(obj, num ,maxlength) {
    var $this = $(obj);
    var v = $this.val();
    var imposeNum = maxlength ? (maxlength+1) : 4;//限制小数点数量，默认为小数点后3位
    if (isNaN(v)) {
        var values = v.slice(0, -1);
        if (isNaN(values)) {
            values = ''
        }
        $this.val(values);
    } else if (v.indexOf('.') != -1) {
        if (v.split('.')[1].length == imposeNum) {
            var values = v.slice(0, -1);
            $this.val(values);
        } else if (v.split('.')[1].length > imposeNum) {
            $this.val('');
        }
    } else {
        $this.val(v.replace(/\s|-/g, ""));
    }

    var newValue = $this.val();
    if (!isNaN(newValue) && newValue != '') {
        if (newValue > num) {
            newValue = num;
        }
    }
    $this.val(newValue);
}

//电话，含座机形式
$("body").on("keyup", ".input-telephone", function () {
    var v = $(this).val();
    var reg = /^[\d-]*$/;
    if (!reg.test(v)) {
        v = v.slice(0, -1);
        $(this).val(v);
    }
    if (!reg.test(v)) {
        $(this).val('');
    }
});
//仅输入中文,字母,数字
function unCharacter(v) {
    return v.replace(/[^\a-\z\A-\Z0-9\u4E00-\u9FA5]/g,'');
}
//禁止特殊字符
function unSpecial(v) {
    return v.replace(/[^u4e00-u9fa5w]/g,'')
}
// 检查是否为手机号
function checkMobile(text) {
    var myreg = /^(1+\d{10})$/;
    return !myreg.test(text) ? false : true;
}
// 检查是否含有特殊字符
function checkSpecail(text) {
    // var myreg = /[@#\$%\^&\*]+/g;
    var regEn = /[`~!@#$%^&*()_+<>?:"{},.\/;'[\]]/im,
        regCn = /[·！#￥（——）：；“”‘、，|《。》？、【】[\]]/im;
    var isTrue = true;
    if(regEn.test(text) || regCn.test(text)) {
        isTrue = false;
    }
    return isTrue;
}

//加载分页
function pageShare(total, curPage, nums, special, oldSize, newSize) {//针对待处理列表分页，页码数，当前页，总条数，特殊标示，每页总条数，实际每次请求数目
    var nowPage = (curPage ? curPage : 1); //实际页码
    $('#page_now').attr('data-r', nowPage);
    $('.page_now').attr('data-r', nowPage);
    $('#go_pre').attr('data-f', oldSize/newSize);
    $('#go_next').attr('data-f', oldSize/newSize);
    $('#go-anypage').attr('data-f', oldSize/newSize);
    $('.totalPagesCount').html(total || '');
    $('#page_now').val(nowPage);
    $('.page_now').html(nowPage);
    if (nowPage == 1) {
        $('#go_pre,#go_pre_cus').addClass('f-ch-un');
    }
    if (nowPage == total) {
        $('#go_next,#go_next_cus').addClass('f-ch-un');
    }
    
    $('.totalElementsCount').html(nums || '');

    $('.footer-table-right').show();
    $('.loading').hide();
}

$('body').on('click', '#go_pre', function () {
    var pages = $('.page_now').html() * 1 - 1;
    if (!$(this).hasClass('f-ch-un')) {
        $('.footer-table-right').hide();
        if ($('body').data('sort')) {
            loadData(pages, $('.nav-tab li.active').attr('data-type'), $('body').attr('data-sort'));
        } else {
            loadData(pages, $('.nav-tab li.active').attr('data-type'), 'total');
        }
    }
})
$('body').on('click', '#go_next', function () {
    var pages = $('.page_now').html() * 1 + 1;
    if (!$(this).hasClass('f-ch-un')) {
        $('.footer-table-right').hide();
        if ($('body').data('sort')) {
            loadData(pages, $('.nav-tab li.active').attr('data-type'), $('body').attr('data-sort'));
        } else {
            loadData(pages, $('.nav-tab li.active').attr('data-type'), 'total');
        }

    }
})
$('body').on('click', '#go-anypage', function () {
    var pages = $('#page_now').val() * 1;

    if (!pages) {
        success_alert('请输入跳转页数', 'error');
    } else {
        $('.footer-table-right').hide();
        if ($('body').data('sort')) {
            loadData(pages, $('.nav-tab li.active').attr('data-type'), $('body').attr('data-sort'));
        } else {
            loadData(pages, $('.nav-tab li.active').attr('data-type'), 'total');
        }
    }
})
$('body').on('keyup', '#page_now', function () {
    var $this = $(this);
    var v = $this.val();
    if (isNaN(v) || (v.indexOf('.') != -1)) {
        var values = v.slice(0, -1);
        if (isNaN(values)) {
            values = ''
        }
        values = values.replace(/\s/g, "");
        $this.val(values);
    } else {  //去除空格
        $this.val(v.replace(/\s/g, ""));
    }

    var newValue = $this.val();
    if (!isNaN(newValue) && newValue != '') {
        if (newValue < 1) {
            newValue = 1;
        } else if (newValue > $('.totalPagesCount').html() * 1) {
            newValue = $('.totalPagesCount').html() * 1;
        }
    }
    $this.val(newValue);
})


// table搜索列表按钮可否有的判断
function panduan() {
    var w = ($('.searchs_top_over').width() || 1150);
    var lisWidth = 0;
    $('#searchs_top_ul li[data-is=1]').each(function () {
        var wli = $(this).outerWidth();
        lisWidth = lisWidth + wli * 1 + 15;
    })
    if (lisWidth < w) {
        $('.next-btn-in').addClass('next-btn-grey')
    } else {
        $('.next-btn-in').removeClass('next-btn-grey')
    }
}
//搜索列表左右按钮
$('.prev-btn-in').on('click', function () {
    var $this = $(this);
    var w = ($('.searchs_top_over').width() || 1150);
    if (!$(this).hasClass('prev-btn-grey')) {
        if ($this.attr('data-do') != '0') {
            $this.attr('data-do', '0');
            var left = parseInt($('#searchs_top_ul').css('margin-left'));

            if (left < -w) {
                left = left + w;
                $('#searchs_top_ul').stop().animate({ marginLeft: left + 'px' }, 500);
            } else {
                $('#searchs_top_ul').stop().animate({ marginLeft: '0px' }, 500, function () {
                    $('.prev-btn-in').addClass('prev-btn-grey');
                    panduan();
                });

            }
            $('.next-btn-in').removeClass('next-btn-grey');
            setTimeout(function () {
                $this.attr('data-do', '1');
            }, 500);
        }

    }

})
$('.next-btn-in').on('click', function () {
    var $this = $(this);
    var w = ($('.searchs_top_over').width() || 1150);
    if (!$(this).hasClass('next-btn-grey')) {
        if ($this.attr('data-do') != '0') {
            $this.attr('data-do', '0');

            var lisWidth = 0;
            var ulWidth = $('#searchs_top_ul').width();
            $('#searchs_top_ul li').each(function () {
                if ($(this).attr('data-is') == 1) {
                    var wli = $(this).outerWidth();
                    lisWidth = lisWidth + wli * 1 + 15;
                }

            })
            if (lisWidth - ulWidth <= w) {
                var left = parseInt($('#searchs_top_ul').css('margin-left'));
                left = -(lisWidth - w);
                $('#searchs_top_ul').stop().animate({ marginLeft: left + 'px' }, 500);
                $('.next-btn-in').addClass('next-btn-grey');
                // panduan();
            } else {
                var left = parseInt($('#searchs_top_ul').css('margin-left'));
                left = left - w;
                $('#searchs_top_ul').stop().animate({ marginLeft: left + 'px' }, 500);
            }

            $('.prev-btn-in').removeClass('prev-btn-grey');

            setTimeout(function () {
                $this.attr('data-do', '1');
            }, 500);
        }

    }
})
// 搜索弹框
function searchDing(obj, type) {
    var $this = $(obj);
    var t = $this.offset().top + 23;
    var l = $this.offset().left;
    var w = $('.' + type).width();
    var bjw = $('.tab-box').offset().left + $('.tab-box').width();
    if (l + w > bjw) {
        l = bjw - w - 10;
        $('.' + type).css({ top: t, left: l });
    } else {
        $('.' + type).css({ top: t, left: l });
    }

    if ($this.attr('value') == '0') {
        $this.next('.unbubble').show();
        $('.searches').attr('value', '0');
        $('.icon_s').removeClass('icon_up');
        $('.icon_s').addClass('icon_down');
        $('.search-list').hide();

        $this.attr('value', '1');
        $this.find('.icon_s').removeClass('icon_down');
        $this.find('.icon_s').addClass('icon_up');
        $('.' + type).show();
        if (type == 'search-list-date') {
            JsClick("search_time");
        }
        $this.attr('data-post', 'no');
    } else {
        $this.attr('value', '0');
        $this.find('.icon_s').removeClass('icon_up');
        $this.find('.icon_s').addClass('icon_down');
        $('.' + type).hide();
    }
    //双日历时间搜索
    if(type && type.indexOf('search-list-times') > -1){
        var afterNum = type.slice(17);
        $('.search-list-times'+afterNum+' .drp-popup').show();
    }
}

// 当前页面对象排序   arr.sort(sortBy(0,key1),sortBy(0,key2))组合排序 arr为[{key1:1,key2:2},{key1:1,key2:2}]的数组对象
var sortBy = function (type, name, minor) { //type=0时正序，1时为倒叙   name为对象内需要排序的key
    return function (o, p) {
        var a, b;
        if (o && p && typeof o === 'object' && typeof p === 'object') {
            a = o[name];
            b = p[name];
            if (a === b) {
                return typeof minor === 'function' ? minor(o, p) : 0;
            }
            if (typeof a === typeof b) {
                if (type == 0) {

                    return a < b ? -1 : 1;
                } else {
                    return b < a ? -1 : 1;
                }
            }
            if (type == 0) {
                return typeof a < typeof b ? -1 : 1;
            } else {
                return typeof b < typeof a ? -1 : 1;
            }

        } else {
            throw ("排序服务错误！");
        }
    }
}
// 用来判断对象是否为空
function isEmptyObject(e) {
    var t;
    for (t in e)
        return !1;
    return !0
}
//处理小数的加减乘除
function floorKeep(num1, num2, type, digits) {
    var num1 = +num1;
    var num2 = +num2;
    var a = num1 + '';
    var b = num2 + '';
    var c1 = parseFloat(a) + '';
    var c2 = parseFloat(b) + '';
    var c = (c1.length > 0 || c2.length > 0) && (c2.length > c1.length) ? c2 : c1;
    c = Math.pow(10, c.length + 1);
    var r = 0;
    var digits = (digits ? digits * 1 : 3);
    if (type == 1) {//减
        r = (Math.round(num1 * c) - Math.round(num2 * c)) / c;
    } else if (type == 2) {//加
        r = (Math.round(num1 * c) + Math.round(num2 * c)) / c;
    } else if (type == 3) {//乘
        r = (Math.round(num1 * c) * Math.round(num2 * c)) / c / c;
    } else if (type == 4) {//除
        r = (Math.round(num1 * c) / Math.round(num2 * c));
    }
    if ((r + '').indexOf('.') > -1 && (r + '').split('.')[1].length > digits) {
        r = r.toFixed(digits)
    }
    return r
}

//操作按钮
$('body').on({
    'mouseenter': function () {
        var l = $(this).offset().left + 22;
        var h = $(this).find('.item-box').outerHeight();
        var w = $(this).find('.item-box').outerWidth();
        var t;
        
        var plus=$(this).parent().parent('td').outerHeight();
        var isleft = $(this).offset().left+w+20;
        var iswidth = $('body').outerWidth();
        if ($(this).offset().top + h + 20 > $(window).height()) {
            t = $(this).offset().top - h + 17;
            $(this).find('.item-box').removeClass('item-box-up').addClass('item-box-down');
        } else {
            t = $(this).offset().top;
            $(this).find('.item-box').removeClass('item-box-down').addClass('item-box-up');
        }
        if(isleft > iswidth){
            l = l - w - 26 - 3;
            $(this).find('.item-box').removeClass('item-box-left').addClass('item-box-right');
        }else{
            $(this).find('.item-box').removeClass('item-box-right').addClass('item-box-left');
        }
        
        $(this).find('.item-box,.line').show().css({ 'left': l, 'top': t });
    }, 'mouseleave': function () {
        $(this).find('.item-box,.line').hide();
    }, 'click': function (e) {
        e.stopPropagation();
    }
}, '.select-item-box');

// 判断是否为数字，包括0
function changeNum(a) {
    if (typeof a == 'number') {
        return a + '';
    }
}

//底部按钮展开隐藏
$("#btn-list .layui-btn").next('.layui-btn-group').hide().children("button").hide();
$(".btn-box").each(function () {
    var self = $(this).find(".btn-focus");
    var parent = $(this);
    var oldw;
    parent.find(".layui-btn-group").css({ "display": "none" });

    self.on('click',function(){
        if(parent.attr('data-open') == 'close'){
            if(self.hasClass("btn-focus-hover")){
                return;
            }
            parent.find(".layui-btn-group").removeAttr("style");
            oldw = parent.find(".layui-btn-group").outerWidth();
            parent.find(".btn-focus").addClass("btn-focus-hover");
            parent.find(".layui-btn-group").css({ "display": "inline-block","width":0});
            parent.find(".layui-btn-group").stop().animate({
                "opacity": 1,
                "width": oldw
            }, 'show');
            parent.attr('data-open','open').attr('title','点击折叠');
            return;
        }
        if(parent.attr('data-open') == 'open'){
            parent.find(".layui-btn-group").stop().animate({
                "opacity": 0,
                "width": 0
            }, 'show', function(){
                self.removeClass("btn-focus-hover");
                parent.find(".layui-btn-group").css({ "display": "none","width": oldw });
            });
            parent.attr('data-open','close');
            return;
        }
    });

});
//切换tab关闭按钮
function closeBtnBox(){
    $(".btn-box").each(function(){
        $(this).attr('data-open','close');
        $(this).find('.btn-focus').removeClass('btn-focus-hover');
        $(this).find('.layui-btn-group').css({"opacity":"0","display":"none"});
    });
}

// 搜索栏刷选按钮展开
$(".btn-pop").hover(function () {
    $(".btn-title").addClass("open");
    $(".btn-list").stop().slideDown('50');
}, function () {
});
$(".btn-pop .close").click(function () {
    $(".btn-list").stop().slideUp('50');
    $(".btn-title").removeClass("open");
});

setTimeout(function(){
	$(".add-org").show();
},500);

//指派时默认选中“需要司机确认”
function getDriverCheckbox(){
    var flag = false;
    $.ajax({
        type: 'post',
        async: false,
        url: '/ttms/config/get_value_list',
        data: {
            keys: 'BATCH_NEED_DRIVER'
        },
        success: function(data){
            if(data.status == 200){
                if(data.body['BATCH_NEED_DRIVER'] == 'true'){
                    flag = true;
                }
                if(data.body['BATCH_NEED_DRIVER'] == 'false'){
                    flag = false;
                }
            }
            else if(data.status == 500){
                success_alert(data.message || '服务器异常，请重试！', 'error');
            }
        }
    });
    return flag;
}
//首字母切换大小写   默认首字母变小写
function letterChange(str,type) {
    var f = str.slice(0,1),l = str.slice(1);
    if(type == 'up'){
        f = f.toLocaleUpperCase();
    }else{
        f = f.toLocaleLowerCase()
    }
    return f+l;
}

//取任意天数日期
function getDayBefore(day){
    var date = new Date(),
        timestamp, newDate, M, D;
    if(!(date instanceof Date)){
        date = new Date(date.replace(/-/g, '/'));
    }
    timestamp = date.getTime();
    newDate = new Date(timestamp - day * 24 * 3600 * 1000);
    M = newDate.getMonth() + 1;
    D = newDate.getDate();
    // return [[newDate.getFullYear(), newDate.getMonth() + 1, newDate.getDate()].join('-'), [newDate.getHours(), newDate.getMinutes(), newDate.getSeconds()].join(':')].join(' ');
    return [newDate.getFullYear(), M, D].join('-');
}


// Hacked global script
document.getElementsByTagName('title')[0].innerText='智运星---专注纯电动汽车优质运力';
